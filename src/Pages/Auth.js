import React, { Component } from 'react'
import styled from 'styled-components'

import { Input, Button } from '../Components/Base'
import Logo from '../img/logo/Invoicer-Logo.png'

class Auth extends Component {
  state = {
    login: true
  }

  toggle = () => {
    this.setState({ login: !this.state.login })
  }

  render() {
    return (
      <div>
        <div style={{ position: 'absolute', top: '10vh', left: '50%', transform: 'translateX(-50%)' }}>
          <img src={Logo} alt="Invoicer Logo"/>
          {
            (this.state.login)
            ? <Login toggle={this.toggle}/>
            : <SignUp toggle={this.toggle}/>
          }
        </div>
        <Nav state={this.state.login} toggle={this.toggle}/>
      </div>
    )
  }
}

const Nav = ({ state, toggle }) => (
  <nav style={{ position: 'fixed', bottom: '10px', top: 'auto', left: '20px' }}>
    <div>
      <Button type="button" 
      primary outline text onClick={toggle}
      style={{ padding: '10px' }}> { state ? 'Create An Account' : 'Log In' } </Button>
    </div>
  </nav>
)

const Help = styled.label`
  font-size: 10px;
  color: #999;
`

const Login = () => (
  <div>
    <form style={{padding: '100px 20px 0px 20px'}}>
      <div className="form-group">
        <Input line full placeholder="Username or Email"/>
        
      </div>
      <div className="form-group">
        <Input type="password" line full placeholder="Password"/>
      </div>
      <div className="form-group d-flex justify-content-between">
        <Button type="button" primary outline text size="14px" style={{marginTop: '-20px'}}> Forgot Password? </Button>
        <Button type="button" primary border style={{ padding: '8px 44px'}}>Sign In</Button>
      </div>
    </form>
  </div>
)

const UserProfile = ({ next }) => (
  <form>
    <div className="form-group">
      <Input line full placeholder="Name"/>
    </div>
    <div className="form-group">
      <Input line full placeholder="Email"/>
      <Help>This email will be used as Username or User ID</Help>
    </div>
    <div className="form-group">
      <Input line full placeholder="Password"/>
    </div>
    <div className="form-group">
      <Input line full placeholder="Confirm Password"/>
    </div>
    <div className="form-group float-right mt-2">
      <Button type="button" primary border style={{ padding: '8px 44px'}} onClick={next}>Continue</Button>
    </div>
  </form>
)

const CompanyDetails = () => (
  <form>
    <div className="form-group">
      <Input line full placeholder="Company Name"/>
    </div>
    <div className="form-group">
      <Input line full placeholder="Address Line 1 / Apartment / Complex"/>
    </div>
    <div className="form-group">
      <Input line full placeholder="Address Line 2 / Locality / Area / Road"/>
    </div>
    <div className="form-group d-flex justify-content-between">
      <Input line full placeholder="City / Town / Village"/>
      <Input type="number" line placeholder="Pincode"/>
    </div>
    <div className="form-group">
      <Input line full placeholder="GSTIN"/>
      <Help>Goods and Services Tax Identification Number</Help>
    </div>
    <div className="form-group float-right mt-2">
      <Button type="button" primary border style={{ padding: '8px 44px'}}>Continue</Button>
    </div>
  </form>
)

const Verification = ({ next }) => (
  <form>
    <div className="form-group">
      <div className="d-flex">
        <Input line full placeholder="Email OTP" style={{ marginRight: '10px' }}/>
        <Button type="button" primary outline text size="14px"> Resend </Button>
      </div>
      <Help>An OTP has been sent to your email. Resend in 10 seconds.</Help>
    </div>
    <div className="form-group">
      <div className="d-flex">
        <Input line full placeholder="Phone OTP" style={{ marginRight: '10px' }}/>
        <Button type="button" primary outline text size="14px"> Resend </Button>
      </div>
      <Help>An OTP has been sent to your phone. Resend in 10 seconds.</Help>
    </div>
    <div className="form-group float-right mt-2">
      <Button type="button" primary border style={{ padding: '8px 44px'}} onClick={next}>Continue</Button>
    </div>
  </form>
)
class SignUp extends Component {

  state = {
    index: 0
  }

  sections = [
    {
      head: 'User Profile',
      component: <UserProfile next={() => this.nextSection()}/>
    },
    {
      head: 'Verify Email And Phone',
      component: <Verification next={() => this.nextSection()}/>
    },
    {
      head: 'Company Details',
      component: <CompanyDetails />
    }
  ]

  nextSection = (data) => {
    this.setState(({index}) => { return  { index: index + 1 } })
  }

  render () {
    const section = this.sections[this.state.index]
    return (
      <div style={{ padding: '100px 20px 0px 20px'}}>
        <h3 style={{fontWeight: 400, marginBottom: '40px'}}>{section.head}</h3>
        <div style={{padding: '5px'}}>
          {section.component}
        </div>
      </div>
    )
  }
}

export default Auth