export function isEmpty (value) {
  if(value) {
    return false
  }
  return true
}

export function nonVoid (value) {
  if (typeof value === 'number') {
    return true
  } else if (typeof value === 'string') {
    return true
  }
  if(value) {
    return true
  }
  return false
}

export function isEmptyObject(obj) {
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop))
        return false
  }
  return JSON.stringify(obj) === JSON.stringify({})
}