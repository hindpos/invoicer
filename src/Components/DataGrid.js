import React, { Component } from 'react'
import styled from 'styled-components'

import { Input } from './Base'

import { isEmpty, nonVoid, isEmptyObject } from '../Helper'

function compute (data, f, define) {
  const copyData = { ...data }
  define.forEach(def => {
    if (nonVoid(def.default)) {
      if (!data[def.key]) {
        copyData[def.key] = def.default
      }
    } 
  })
  return f(copyData)
} 

export class DataGrid extends Component {

  constructor(props) {
    super(props)
    this.prevIndices = this.prevMutableIndices()
    }

  computeGrid = () => {
  
  }
  
  renderGridBody = () => {
  
  }

  prevMutableIndices = () => {
    const mutableIndices = this.props.define
      .map((def, i) => [def, i])
      .filter(def => def[0].mutable)
      .map(d => d[1])
    const prevIndices = mutableIndices
      .map((index) => mutableIndices[(index - 1 + mutableIndices.length) % mutableIndices.length])
    const iterable = mutableIndices.map(i => [i, prevIndices[i]])
    return new Map(iterable)
  }

  keyHandler = (event) => {
    const { data, define } = this.props
    const { target, key } = event

    const [ rowN, colN ] = target.parentNode.getAttribute('data-path')
      .split(',')
      .map(l => parseInt(l, 10))

    const isLast = rowN === data.length

    if (key === 'Enter') {
      if (event.shiftKey) {
        if (rowN !== 0) {
          const element = document.querySelector(`div[data-path="${rowN-1},${colN}"] > input`)
          element.focus()
        }
      } else {
        if (rowN !== data.length) {
          const element = document.querySelector(`div[data-path="${rowN+1},${colN}"] > input`)
          element.focus()
        }
      }
    }

    if (key === 'Backspace') {
      const def = define[colN]

      const prevColN = this.prevIndices.get(colN)
      let row = rowN
      if (prevColN > colN && rowN > 0) {
        row = rowN - 1
      }

      if (row === 0 && rowN === 0 && colN === 0) {
        return
      }

      if (!isLast) {
        const value = data[rowN][def.key]
        const isVoid = isEmpty(value)
        // equals the default if present
        || ((nonVoid(def.default) && def.mutable) && (def.default === value))
        if (isVoid) {
          const element = document.querySelector(`div[data-path="${row},${prevColN}"] > input`)
          element.focus()
          event.preventDefault()
        }
      } else {
        const element = document.querySelector(`div[data-path="${row},${prevColN}"] > input`)
        element.focus()
        event.preventDefault()
      }
    }
  }

  onChange = (rowN, colN, value, event) => {
    const  { data, define } = this.props

    // IF last row, create a row in the data
    const isLastRow = rowN === data.length
    if (isLastRow) {
      data[rowN] = {}
      define.forEach((d, i) => {
        if (nonVoid(d.default) && i !== colN) {
          data[rowN][d.key] = d.default
        }
      })
    }

    // last row do nothing
    if(rowN === data.length) {
      return
    }

    // Casting if required
    const { cast, key } = define[colN]
    let val = cast ? cast(value) : value
    // Special case when val is NaN
    if (!val) {
      delete data[rowN][key]
    // every other case
    } else {
      data[rowN][key] = val
    }

    // Deleting Row if empty
    const isEmptyRow = define
      .map((d, i) => 
        // if the cell is empty
        isEmpty(data[rowN][define[i].key])
        // equals the default if present
        || ((nonVoid(d.default) && d.mutable) && (d.default === data[rowN][define[i].key]))
      )
      .reduce((a,c) => a && c)
    if (isEmptyRow) {
      data.splice(rowN, 1)
    }

    this.props.updateData(data)
  }

  render() {
    const totalWidth = this.props.define.map(def => def.width || 1).reduce((a, c) => a+c)
    const overWidth = totalWidth > 12
    return (
      <div className="mb-3" style={{borderTop : '2px dashed #eee'}} onKeyDownCapture={this.keyHandler}>
        {
          [...this.props.data, {}]
            .map((row, index) => 
              <Row 
              key={index}
              change={this.onChange}
              define={this.props.define}
              row={row} 
              index={index}
              overWidth={overWidth}
              totalWidth={totalWidth}/>)
        }
      </div>
    )
  }
}

const Cell = ({ define, row, path, empty, overWidth, totalWidth, change, defineList }) => {
  const widthClass = `col-${define.width || 1}`
  let value

  if (empty) {
    value = ''
  } else {
    if (define.mutable) {
      value = row[define.key]
    } else {
      value = compute(row, define.compute, defineList)
    }
    if (define.decorator) {
      value = define.decorator(value)
    }
  }

  const content = empty
    ? define.mutable
      ? <Input placeholder={define.placeholder} className="w-100" value={''} onChange={e => change(...path, e.target.value, e)}/>
      : <Thin>{define.placeholder}</Thin>
    : define.mutable
      ? <Input placeholder={define.placeholder} className="w-100" value={nonVoid(value) ? value : (typeof value === 'number' ? 0 : '')} onChange={e => change(...path, e.target.value, e)}/>
      : value
  return (
    <div data-path={path}
    className={overWidth ? 'p-1' : `${widthClass} p-1`}
    style={{ ...define.style, 
      minWidth: overWidth ? `${define.width*50}px`: 'auto',
      width: overWidth ? `${define.width / totalWidth}%`: 'auto' }}>
        {content}
      </div>
  )
}

const Row = ({ define, row, index, overWidth, totalWidth, change}) => {
  let content
  if(isEmptyObject(row)) {
    content = define.map((def, j) => <Cell change={change} totalWidth={totalWidth} overWidth={overWidth} defineList={define} define={def} row={row} empty={true} key={j} path={[index, j]}/>)
  } else {
    content = define.map((def, j) => <Cell change={change} totalWidth={totalWidth} overWidth={overWidth} key={j} defineList={define} define={def} row={row} path={[index, j]}/>)
  }
  return (
    <div className="row py-2 mr-0 ml-0"
    style={{ minWidth: overWidth ? `${totalWidth*50}px`: 'auto',
     borderBottom: '2px dashed #eee' }}>
      {content}
    </div>
  )
}

const Thin = styled.span`
  font-weight: 300;
  color: #999;
`