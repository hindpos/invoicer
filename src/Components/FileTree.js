import React, { Component } from 'react'
// eslint-disable-next-line
import * as Icon from 'react-feather'

import './FileTree.css'
import { Input } from './Base'

export class FileTree extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
      path: props.path || []
    }
    this.props.onChange(openFile(props.files, props.path))
  }

  clickHandler = (event) => {
    event.stopPropagation()
    const path = event.currentTarget.getAttribute('data-path').split(',').map(l => parseInt(l))
    this.toggle(path)
  }

  toggle = (path) => {
    const { files } = this.props
    let node = files[path[0]]
    for (let index = 1; index < path.length; index++) {
      const selector = path[index]
      node = node.contents[selector]
    }
    node.open = !node.open
    if (node.type === 'd') {
      this.props.onChange(files)
    } else {
      this.props.onChange(files, path)
    }
  }

  closeFiles = () => {
    const { files } = this.props
    Object.keys(files).forEach(key => closeFile(files[key]))
    return files
  }

  resetFiles = () => {
    let files = this.closeFiles()
    files = openFile(files, this.props.path)
    return files
  }

  searchFiles = (event) => {
    let { files } = this.props
    const needle = event.target.value
    this.setState({ search: needle })
    if (needle.length > 0) {
      const paths = findNodesAggregator(this.props.files, needle)
      for (let index = 0; index < paths.length; index++) {
        const path = paths[index]
        files = openFile(files, path)
      }
      this.props.onChange(files)
    } else {
      files = this.resetFiles()
      this.props.onChange(files)
    }
  }

  render() {
    return (
        <div className="FileTree">
          <div className="input-group flex-nowrap mb-3">
            <div className="input-group-prepend">
              <span className="input-group-text border-0" style={{ background: 'white'}}>
              <Icon.Search size="20px" />
              </span>
            </div>
            <Input className="mr-4 p-1 w-100 rounded" size="16px"placeholder="Search"
            value={this.state.search} onChange={this.searchFiles}/>
          </div>
          {renderFileTree(this.props.files, this.clickHandler)}
        </div>
      )
  }
}

function renderFileTree(files, clickHandler, depth = 0, path = []) {
  return (
    <ul className="ml-3" style={{ listStyle: 'none' }}>
      {Object.keys(files).map((key, index) => {
        const file = files[key]
        const pathUpdated = append(path, key)
        const open = isOpen(file)
        return (
        <li key={index} data-path={pathUpdated} data-depth={depth}
        onClick={clickHandler}>
          {
            (file.type === 'd')
            ? (
                <div className="dir">
                  <Dir open={open} name={file.name} />
                  {open ? renderFileTree(file.contents, clickHandler, depth+1, pathUpdated) : null}
                </div>
              )
            : (file.type === 'c')
            ? (
                <div>
                  <Container name={file.name} number={file.contents.length} open={open}/>
                  {open ? renderFileTree(file.contents, clickHandler, depth+1, pathUpdated) : null}
                </div>
              )
            : (
                <File open={open} name={file.name} />
              )
          }
        </li>)})}
    </ul>
  )
}

function openFile(files, path) {
  if (path.length === 0) {
    return files
  }
  let node = files[path[0]]
    for (let index = 1; index < path.length; index++) {
      node.open = true
      const selector = path[index]
      node = node.contents[selector]
    }
    return files
}

function closeFile(file) {
  if (file.type === 'd') {
    file.open = false
    Object.keys(file.contents).forEach(key => {
      closeFile(file.contents[key])
    })
  }
}

const findNodesAggregator = (trees, needle) => {
  return Object.keys(trees)
          .map(key => findNodes(trees[key], [key], needle))
          .reduce((a, c) => a.concat(c), [])
}

const findNodes = (tree, path, needle) => {
  let hay = []
  if (tree.type === 'd') {
    if (tree.name.includes(needle)) {
      hay.push(path)
    }
  } else {
    if (tree.name.includes(needle)) {
      return [path]
    }
  }
  return Object.keys(tree.contents)
          .map(key => findNodes(tree.contents[key], append(path, key), needle))
          .reduce((a, c) => a.concat(c), [])
          .concat(hay)
}

const Dir = ({ open, name }) => {
  return (
    <div>
      <FileIcon open={open} />
      <span className="ml-1 FileName position-relative">{name}</span>
    </div>
  )
}

const File = ({ name, open }) => {
  return (
    <div className="file">
      <div>{name}</div>
    </div>
  )
}

const Container = ({ name, open, number }) => {
  return (
    <div className="Container">
      <div>{name}</div>
      {open ? null : <div>{`${number} ${(number > 1) ? 'Invoice' : 'Invoices'}`}</div>}
    </div>
  )
}

function append (arr, item) {
  const newArr = arr.slice()
  newArr.push(item)
  return newArr
}

function isOpen(file) {
  if (file.open) {
    return true
  }
  return false
}

export class FileIcon extends Component {
  render() {
    return (
      <div className="d-inline FileIcon">
        <FileBack size={this.props.size} />
        {
          this.props.open
          ? (<FileFrontOpen size={this.props.size} />)
          : (<FileFrontClose size={this.props.size} />)
        }
      </div>
    )
  }
}


const FileBack = ({ size = 25 }) => {
  const width = `${size}px`
  const height = `${parseInt(size * 4 / 5)}px`
  return (
    <div className="d-inline position-absolute">
      <svg width={width} height={height} viewBox="0 0 200 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <title>FileBack</title>
        <g id="FileBack" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <path d="M90.7186058,13.1150696 L107.148983,32.1264423 C109.618375,34.9837436 113.208232,36.6259873 116.984747,36.6259873 L176,36.6259873 C178.209139,36.6259873 180,38.4168483 180,40.6259873 L180,156 C180,158.209139 178.209139,160 176,160 L24,160 C21.790861,160 20,158.209139 20,156 L20,14 C20,11.790861 21.790861,10 24,10 L83.9092304,10 C86.5237411,10 89.0090265,11.1369379 90.7186058,13.1150696 Z" id="back" fill="#7BB9FB"></path>
        </g>
      </svg>
    </div>
  )
}

const FileFrontClose = ({ size = 25 }) => {
  const width = `${size}px`
  const height = `${parseInt(size * 4 / 5)}px`
  return (
    <div className="d-inline position-relative">
      <svg width={width} height={height} viewBox="0 0 200 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <title>FileFrontClose</title>
        <g id="FileFrontClose" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <rect id="fornt" fill="#0077FF" x="20" y="50" width="160" height="110" rx="4"></rect>
        </g>
      </svg>
    </div>
  )
}

const FileFrontOpen = ({ size = 25 }) => {
  const width = `${size}px`
  const height = `${parseInt(size * 4 / 5)}px`
  return (
    <div className="d-inline position-relative">
      <svg width={width} height={height} viewBox="0 0 200 160" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <title>FileFrontOpen</title>
        <g id="FileFrontOpen" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <path d="M7.00073573,66 L193.323469,66 C195.532608,66 197.323469,67.790861 197.323469,70 C197.323469,70.2524537 197.299569,70.5043404 197.252089,70.7522891 L180.784004,156.752289 C180.423083,158.637103 178.774442,160 176.855383,160 L23.4688208,160 C21.5497621,160 19.9011218,158.637103 19.5402001,156.752289 L3.07211496,70.7522891 C2.65663717,68.5825717 4.07872931,66.486857 6.24844665,66.0713792 C6.49639532,66.0238997 6.74828207,66 7.00073573,66 Z" id="fornt" fill="#0077FF"></path>
        </g>
      </svg>
    </div>
  )
}