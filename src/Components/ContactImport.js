import React from 'react'
import shortid from 'shortid'
import vCardParser from 'vcard-parser'

import { Upload } from 'Components/Base'
import CSV from 'Util/CSV'

const ContactImport = ({ update }) => {
  let fileReader
  let fileType

  const CSVFileType = 'text/csv'
  const VCFFileType = 'text/vcard'
  const AcceptedFileType = [CSVFileType, VCFFileType]

  const processCSV = (fileData) => {
    const csv = new CSV(fileData)
    const contacts = csv.parse()
      .map(contact => {
        const formattedContact = {}
        formattedContact.id = shortid.generate()
        formattedContact.name = (contact.name || '').trim()
        if (!formattedContact.name) {
          return null
        }

        if (contact['Phone 1 - Value']) {
          formattedContact.phone = contact['Phone 1 - Value']
            .split(':::')[0]
            .trim()
            .replace(/[^\w+]/gi, '')
          formattedContact.whatsapp = formattedContact.phone
        }

        if (contact['E-mail 1 - Value']) {
          formattedContact.email = contact['E-mail 1 - Value']
            .split(':::')[0]
            .trim()
            .toLowerCase()
        }
        return formattedContact
      })
      .filter(c => c)
    update(contacts)
  }

  const processVCF = (fileData) => {
    const contacts = fileData
      .split('END:VCARD')
      .map(t => t+'END:VCARD')
      .map(t => vCardParser.parse(t))
      .slice(0, -1)
      .map(c => {
        const formattedContact = {}
        if (!c) {
          return null
        } else if (!c.fn) {
          return null
        } else if (c.fn.length === 0) {
          return null
        } else if (!(c.fn[0].value)) {
          return null
        }
        formattedContact.name = c.fn[0].value
        if (c.tel) {
          if (c.tel.length > 0) {
            if (c.tel[0].value) {
              formattedContact.phone = c.tel[0].value
                .split(':::')[0]
                .trim()
                .replace(/[^\w+]/gi, '')
              formattedContact.whatsapp = formattedContact.phone

              // Checking whatsapp contact
              if (c.tel.length > 1) {
                if (c['X-ABLabel']) {
                  let WhatsAppNamespace
                  let isWhatsApp = c['X-ABLabel']
                    .map(c => {
                      if (c.value) {
                        return c.value.trim().toLowerCase() === 'whatsapp'
                        WhatsAppNamespace = c.namespace
                      }
                      return false
                    })
                    .reduce((a, c) => a || c)
                  
                  if (isWhatsApp) {
                    for (let index = 0; index < c.tel.length; index++) {
                      const tel = c.tel[index]
                      if (tel.namespace) {
                        if (tel.namespace === WhatsAppNamespace) {
                          formattedContact.whatsapp = tel.value
                            .split(':::')[0]
                            .trim()
                            .replace(/[^\w+]/gi, '')
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        if (c.email) {
          if (c.email.length > 0) {
            if (c.email[0].value) {
              formattedContact.email = c.email[0].value
              .split(':::')[0]
              .trim()
              .toLowerCase()
            }
          }
        }
        return formattedContact
      })
      .filter(c => c)
    console.log(contacts)
    // update(contacts)
  }

  const process = {
    [CSVFileType]: processCSV,
    [VCFFileType]: processVCF
  }

  const processFile = () => {
    const fileData = fileReader.result
    const processor = process[fileType]
    try {
      processor(fileData)
    } catch (e) {
      alert ("Corrupt File or File couldn't be read")
    }
  }

  const onChange = (event) => {
    const file = event.target.files[0]
    if (!file) {
      return
    }

    fileType = file.type
    if (file.name.indexOf('.vcf') > 0) {
      fileType = VCFFileType
    }

    // Checking if wrong file type
    if (AcceptedFileType.indexOf(fileType) === -1) {
      alert('Only vCard(iOS) and CSV(Google Contacts) formats are accepted')
      return
    }

    // Read file
    fileReader = new FileReader()
    fileReader.onloadend = processFile
    fileReader.readAsText(file)
  }

  return (
    <Upload title={'+ Import'} onChange={onChange}/>
  )
}

export default ContactImport