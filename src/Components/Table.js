import React, { Component } from 'react'

import { DataGrid } from './DataGrid'
import { nonVoid } from '../Helper'


export class Table extends Component {

  updateData = (data) => {
    this.props.updateTable({define: this.props.table.define, data})
  }

  render() {
    return  (
      <div>
        <DataGrid data={this.props.table.data} define={this.props.table.define} updateData={this.updateData}/>
        <SubTable subTable={this.props.subTable} data={this.props.table.data} define={this.props.table.define}/>
      </div>
    )
  }
}

const SubTable = ({ subTable, data, define }) => {
  const items = subTable
                  .map(({style, compute, title}) => ({ style, title, value: defaultCompute(data, compute, define) }))
                  .map(({ title, value, style}, index) => (
                    <tr key={index} style={{ borderBottom: '2px dashed #eee', textTransform: 'uppercase' }}>
                      <td>{title}</td>
                      <td style={style} className="float-right">{value}</td>
                    </tr>
                  ))
  return (
    <table className="table table-borderless mt-4">
      <tbody style={{fontWeight: 400, color: '#aaaaaf'}}>
        {items}
      </tbody>
    </table>
  )
}

function defaultCompute (data, f, define) {
  const defaultData = data.map(row => {
    const copyRow = {...row}
    define.forEach(def => {
      if (nonVoid(def.default)) {
        if (!row[def.key]) {
          copyRow[def.key] = def.default
        }
      }
    })
    return copyRow
  })
  return f(defaultData)
}