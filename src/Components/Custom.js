import React from 'react'
import styled from 'styled-components'

export { Table } from './Table'

const Circle = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 100%;
  text-align: center;
  vertical-align: middle;
  line-height: 40px;
  color: ${props => props.color};
  background: ${props => props.background};
`

export const Contact = ({ contact }) => {
  const colors = randomColorPair()
  return (
    <div className="user d-flex mt-4">
      <div>
        <Circle color={colors.dark} background={colors.light}>{contact.name.charAt(0).toUpperCase()}</Circle>
      </div>
      <div className="username pl-2 flex-grow-1">
        <h6 className="m-0">{contact.name}</h6>
        <div style={{ color: '#aaaaaf'}}>
          <span style={{ fontSize: '12px', fontWeight: '400'}}>via </span>
          <span style={{ fontSize: '12px', fontWeight: '500'}}>{contact.type}</span>
          <span style={{ fontSize: '12px', fontWeight: '400'}}> to </span>
          <span style={{ fontSize: '12px', fontWeight: '500'}}>{contact.address}</span>
        </div>
      </div>
    </div>
  )
}

const colors = [
  {light: '#ffcdd2', dark: '#f44336'},
  {light: '#f8bbd0', dark: '#e91e63'},
  {light: '#e1bee7', dark: '#9c27b0'},
  {light: '#d1c4e9', dark: '#673ab7'},
  {light: '#c5cae9', dark: '#3f51b5'},
  {light: '#bbdefb', dark: '#2196f3'},
  {light: '#b3e5fc', dark: '#03a9f4'},
  {light: '#b2ebf2', dark: '#80deea'},
  {light: '#b2dfdb', dark: '#009688'},
  {light: '#c8e6c9', dark: '#4caf50'},
  {light: '#dcedc8', dark: '#8bc34a'},
  {light: '#f0f4c3', dark: '#cddc39'},
  {light: '#fff9c4', dark: '#ffeb3b'},
  {light: '#ffecb3', dark: '#ffc107'},
  {light: '#ffe0b2', dark: '#ff9800'},
  {light: '#ffccbc', dark: '#ff5722'},
  {light: '#d7ccc8', dark: '#795548'},
  {light: '#f5f5f5', dark: '#9e9e9e'},
  {light: '#cfd8dc', dark: '#607d8b'}
]

const randomColorPair = () => {
  const seed = Math.floor(Math.random() * colors.length)
  return colors[seed]
}