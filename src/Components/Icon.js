import React from 'react'

import Mail from '../img/logo/Mail.png'
import WhatsApp from '../img/logo/WhatsApp.png'
import Messages from '../img/logo/Messages.png'

export class Icon extends React.Component {
  icons = {
    Mail,
    WhatsApp,
    Messages
  }
  render () {
    return (
      <div className="Icon_ d-inline rounded">
        <img src={this.icons[this.props.icon]}
        alt={this.props.icon}
        style={{ height: `${this.props.size || 25}px`}} />
      </div>
    )
  }
}