import React, { Component } from 'react'
import styled, { css } from 'styled-components'

// eslint-disable-next-line
Array.prototype.intersect = function(a) {
  return this.filter(function(i) {
    return a.indexOf(i) > -1;
  })
}

export const Button = styled.button`
  display: block;
  text-align: center;
  vertical-align: middle;
  user-select: none;
  line-height: 1.5;
  border-radius: .25rem;
  transition: .15s all ease-in-out;
  font-weight: 500;
  padding: 8px 16px;
  font-size: ${props => props.size || '1rem' };

  ${props =>
    props.border &&
    css`
      border-width: 1px;
      border-style: solid;
    `}

  ${props =>
    props.primary &&
    css`
      border-color: #1e93ff;
      background: ${props => (props.outline ? "#eff6fd" : "#1e93ff")};
      color: ${props => (props.outline ? "#1e93ff" : "white")};
    `}

  ${props =>
    props.text &&
    css`
      background: transparent;
      border: none;
      padding: 0;
    `}

  ${props => props.danger &&
    css`
      color: red;
    `}
`;

export const Input = styled.input`
  border: none;
  padding: 0;
  margin: 0;

  padding-left: 3px;

  font-weight: ${props => props.weight || 300};

  font-size: ${props => props.size};
  color: ${props => props.color || '#222'};

  ${props => props.line && css`
    border-bottom: 1px solid #dedede;
  `}

  ${props => props.full && css`
    width: 100%
  `}

  transition: .5s ease-in-out;

  :hover,
  :focus,
  :active {
    outline: none;
  }
  :focus {
    color: #1e93ff;
  }

  ::placeholder {
    color: ${props => props.color || '#999'};
  }

  :focus {
    ::placeholder {
      color: ${props => props.color || '#999'};
    }
  }
`;

export const Title6 = styled.h6`
  color: #aaaaaf;
  font-size: 15px;
`;

const Card = styled.div`
  position: absolute;
  padding: 5px;
  background: white;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

export class Popup extends Component {
  state = {
    viewIndex: 0
  }

  nextView = () => {
    this.setState ({
      viewIndex: this.state.viewIndex + 1
    })
  }
  
  render () {
    return (
      <Card>
        {this.props.views[this.state.viewIndex]}
        { this.state.viewIndex < this.props.views.length - 2
        ? (<button onClick={this.nextView}>Continue</button>) 
        : (<button onClick={this.props.destroy}>Done</button>) }
      </Card>
    )
  }
}

Popup.defaultProps = {
  views: [
    (
      <div>
        <h1>First view</h1>
        <p>this is first view</p>
      </div>
    ),
    (
      <div>
        <h1>Second view</h1>
        <p>this is second view</p>
      </div>
    ),(
      <div>
        <h1>Third view</h1>
        <p>this is third view</p>
      </div>
    ),(
      <div>
        <h1>Third view</h1>
        <p>this is third view</p>
      </div>
    )
  ],
  destory: () => {}
}

export const Upload = ({ title, onChange }) => (
  <div className='position-relative overflow-hidden d-inline-block'>
    <Button primary outline text>{title}</Button>
    <input type="file" name="myfile" onChange={e => onChange(e)}
    className='position-absolute' style={{ left: 0, top: 0, opacity: 0, fontSize: '100px' }} />
  </div>
)