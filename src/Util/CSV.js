class CSV {
  constructor(data) {
    this.data = data
  }
  parse(separator = ',', lineBreak= '\n') {
    const lines = this.data.split(lineBreak)
    const body = lines.slice(1).map(row => row.split(separator))
    const headers = lines[0].split(separator)

    const json = []
    body.forEach((row, rowN) => {
      const jsonRow = {}
      row.forEach((cell, cellN) => {
        const key = headers[cellN]
        jsonRow[key] = cell
      })
      json.push(jsonRow)
    })
    return json
  }
}

export default CSV