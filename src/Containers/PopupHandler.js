import React, { Component } from 'react'

import { Popup } from '../Components/Base'

export class PopupHandler extends Component {
  render() {
    return (
      <Popup />
    )
  }
}