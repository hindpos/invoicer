import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as Icon from 'react-feather'

import Preferences from './Preferences'

import Logo from '../img/logo/Invoicer-Logo.png'
import { FileTree } from '../Components/FileTree'

import User from '../Dummy/User'

class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      preferences: true,
      path: [1, 7]
    }
  }

  toggle = () => {
    this.setState(({preferences}) => ({preferences: !preferences}))
  }

  update = (files, path) => {
    if (files) {
      this.setState({files})
    }
    if (path) {
      this.setState({path})
    }
  }

  fileTree = () => {
    if (!this.state.preferences) {
      return (
        <div className="file-tree flex-grow-1 mt-5 pl-4">
          <FileTree
          files={this.props.files}
          onChange={this.update}
          path={this.state.path}/>
        </div>
      )
    }
  }

  render() {
    return (
      <aside className="sidebar d-flex flex-column py-4"
      style={{ 
        width: '300px',
        background: '#f9fafb',
        height: '100vh'
      }}>
        <h4 className="logo px-4">
          <img src={Logo} alt="" style={{ height: '35px' }} />
        </h4>
        { this.fileTree() }
        <PreferencesList user={User} expanded={this.state.preferences} toggle={this.toggle}/>
      </aside>
    )
  }
}

const PreferencesList = ({ user, expanded, toggle, preferences }) => {
  if (expanded) {
    return <PreferencesExpanded user={user} collapse={toggle} preferences={preferences}/>
  }
  return <PreferencesCollapsed user={user} expand={toggle}/>
}

class PreferencesExpanded extends Component {
  constructor(props) {
    super(props)
    this.state = {
      preference: 'Contacts'
    }
  }

  prefControl = (preference) => {
    this.setState({ preference })
  }

  prefsList = Object.keys(Preferences)
                .map((p, i) => <li onClick={() => this.prefControl(p)} className='mt-1' key={i}>{p}</li>)

  render () {
    const { user, collapse } = this.props
    const { preference } = this.state

    if (preference) {
      return (
        <div className='d-flex flex-column justify-content-between mt-4 pt-4'
        style={{height: '100%'}}>
          <div className='mx-4'>
            <h3>{preference}</h3>
            {Preferences[preference]()}
          </div>
          <div className='mx-auto' onClick={() => this.prefControl()}>
            <Icon.X color="#aaaaaf"/>
          </div>
        </div>
      )
    }
    
    return (
      <div className='d-flex flex-column justify-content-between mt-4 pt-4 mx-auto'
      style={{height: '100%'}}>
        <div className='text-center'>
          <ProfilePic user={user} size={160}/>
          <div className='mt-4' style={{fontSize: '18px', fontWeight: 500, lineHeight: '25px', color: '#222'}}>{user.name}</div>
          <div style={{color: '#aaaaaf', fontSize: '15px'}}>{user.invoices} Invoices</div>
          <ul className='text-left p-0' style={{ listStyle: 'none', marginLeft: '-20px', marginTop: '50px', color: '#1e93ff' }}>
            {this.prefsList}
          </ul>
        </div>
        <div className='mx-auto' onClick={collapse}>
          <Icon.X color="#aaaaaf"/>
        </div>
      </div>
    )
  }
}

const PreferencesCollapsed = ({ user, expand }) => {
  return (
    <div className='d-flex px-4' onClick={expand}>
      <ProfilePic user={user}/>
      <div className='d-flex justify-content-between pl-3 pt-1 w-100'>
        <div>
          <div style={{fontSize: '18px', fontWeight: 500, lineHeight: '25px', color: '#222'}}>{user.name}</div>
          <div style={{color: '#aaaaaf', fontSize: '15px'}}>{user.invoices} Invoices</div>
        </div>
        <Icon.CornerRightUp color="#aaaaaf"/>
      </div>
    </div>
  )
}

const ProfilePic = ({ user: { img, name }, size = 60 }) => {
  const sizePX = `${size}px`
  const halfSizePX = `${size/2}px`
  if (img) {
    return (
      <img alt='Profile Pic'
      src={img} className='rounded-circle' style={{
        width: sizePX,
        height: sizePX
      }}/>
    )
  }
  return (
    <div className='text-center rounded-circle'
    style={{
      fontSize: halfSizePX,
      background: '#7BB9FB',
      color: '#0077ff',
      height: sizePX,
      width: sizePX,
      lineHeight: sizePX
    }}>
      {name.charAt(0)}
    </div>
  )
}

function mapStateToProps(state) {
  return { files: state.fileTree }
}

export default connect(mapStateToProps)(Sidebar)
