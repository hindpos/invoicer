import React from 'react'

import Contacts from './Contact'
import Profile from './Profile'
import Reports from './Reports'
import Settings from './Settings'
import Templates from './Templates'

export default {
  Contacts: () => <Contacts/>,
  Profile: () => <Profile/>,
  Reports: () => <Reports/>,
  Settings: () => <Settings/>,
  Templates: () => <Templates/>
}