import React, { Component } from 'react'
import { connect } from 'react-redux'
import vCardParser from 'vcard-parser'

import { Input, Button } from 'Components/Base'
import ContactImport from 'Components/ContactImport'

import { SAVE_CONTACT, CLEAR } from 'Store/Constants/actionTypes'

class Contact extends Component {
  state = {
    needle : ''
  }

  updateFilter = (event) => {
    this.setState({needle : event.target.value})
  }

  updateContactList = (contacts) => {
    const { save } = this.props
    contacts.forEach((contact) => save({contact}))
  }

  export = () => {
    

    // const { contacts } = this.props
    // let text = 'Name;Phone;WhatsApp;Email\n'
    // contacts.forEach(contact => {
    //   text += `${contact.name || ''};${contact.phone || ''};${contact.whatsapp || ''};${contact.email}\n`
    // })
    // download('Contacts.csv', text)
  }

  render() {
    const { contacts } = this.props
    const { needle } = this.state
    let filteredContacts = contacts
    
    if (needle) {
      filteredContacts = contacts
      .filter((contact) => contact.name
        .split(' ')
        .map(part => part.indexOf(needle) === 0)
        .reduce((a, c) => a || c))
    }

    filteredContacts = filteredContacts
      .sort((el1, el2) => el1.name > el2.name)
    
    return (
      <div>
        <div className='position-relative float-right'>
          
        </div>
        <div className='d-flex justify-content-between my-2 py-3 border-bottom border-top'>
          <Button primary outline text onClick={this.export}>Export</Button>
          <ContactImport update={this.updateContactList}/>
          <Button primary outline text danger onClick={this.props.clear}>Clear</Button>
        </div>
        <div className='pt-2'>
          <Input className='p-1 w-100 rounded' size='16px' placeholder='Search'
          value={this.state.needle} onChange={this.updateFilter}/>
          <ul className='p-0 mt-3 px-1'
          style={{listStyle: 'none', fontWeight: 500, height: 'calc(100vh - 330px)', overflow: 'scroll'}}>
            {filteredContacts.map((contact, index) => <li className='p-1' key={index}>{contact.name}</li>)}
          </ul>
        </div>
      </div>
    )
  }
}

function download(filename, text) {
  var element = document.createElement('a')
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text))
  element.setAttribute('download', filename)

  element.style.display = 'none'
  document.body.appendChild(element)

  element.click()

  document.body.removeChild(element)
}

const mapStateToProps = (state) => ({
  contacts: state.contacts.contactList,
  contactTypes: state.contactTypes
})

const mapDispatchToProps = dispatch => ({
  save: payload => dispatch({ type: SAVE_CONTACT, payload }),
  clear: () => dispatch({ type: CLEAR })
})

export default connect(mapStateToProps, mapDispatchToProps)(Contact)