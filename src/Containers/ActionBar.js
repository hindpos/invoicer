import React, { Component } from 'react'
import { connect } from 'react-redux'
import shortid from 'shortid'

import { Icon } from '../Components/Icon'
import { Button, Input } from '../Components/Base'
import { SAVE_CONTACT } from 'Store/Constants/actionTypes'

const RecContactBox = ({contacts }) => (
 contacts.map(c => (
    <div key={c.id} className='mt-3 px-2'>
      <Icon icon={c.type} style={{ paddingRight: '5px'}} />
      <span className="px-2">{c.name}</span>
    </div>
  ))
)

class SearchBox extends Component {
  state = {
    filter: '',
    focus: false,
  }
  refs = {
    input: React.createRef(),
  }
  hc = (ev, type) => {
    this.setState({
      [type]: ev.target.value
    })
  }
  _onFocus = () => {
    this.setState({
      focus: true
    })
  }
  _onBlur = () => {
    this.setState({
      focus: false
    })
  }
  FilteredOldContacts = ({ data: 
    {contacts, oldContacts, filter, focus,
      control: { onOldContactClick } 
    }}) => {
    const contactIds = contacts.map(c => c.id)
    const filterF = (contact) => contact.name
      .split(' ')
      .map(part => part.indexOf(filter) === 0)
      .reduce((a, c) => a || c)
    return (
      oldContacts
        .map(c => contactIds.indexOf(c.id) < 0 && filterF(c)).reduce((acc, nxt) => acc || nxt , false) && focus
      ? <ul className='position-absolute pl-0 border mt-3' 
        style={{
          zIndex: 1,
          background: 'white',
          minWidth: '296px',
          cursor: 'pointer'
        }}>{
          oldContacts
            .filter(c => contactIds.indexOf(c.id) < 0 && filterF(c))
            .slice(0,5)
            .map(c => (
              <li style={{ listStyle: 'none'}}
                key={c.id}
                className='d-flex justify-content-between py-2 px-3 active'
                onClick={(ev) => {
                  this.setState(({focus}) => ({
                    focus: !focus
                  }))
                  onOldContactClick(ev, c)
                }}>
                <span>{c.name}</span>
              </li>
            ))
          }
        </ul>
      : null)
  }
  render() {
    const {
      props: {contacts, oldContacts, control},
      state: { filter, focus },
    } = this
    return(
      <form>
        <Input
          className='p-1 mt-3 w-100 mr-3'
          size='16px'
          placeholder='Sending to'
          value={filter}
          ref={this.refs['input']}
          onChange={(e) => this.hc(e, 'filter')}
          onFocus={this._onFocus}
        />
        <this.FilteredOldContacts data={{contacts, oldContacts, filter, control, focus}}/>
      </form>
    )
  }
}

class NewContactBox extends Component {
  state = {
    Name: '',
    Messages: '',
    WhatsApp: '',
    Mail: '',
  }
  hc = (ev, type) => {
    const val = ev.target.value
    this.setState({
      [type]: val
    })
    if (type === 'Messages' && this.state['WhatsApp'] === val.substring(0, val.length-1)) {
      this.setState({
        WhatsApp: val
      })
    }
  }
  ContactInput = ({fields}) => (
    <form>
      {fields.map((f) => (
        <Input
          type={(f.id === 'Name' || f.id === 'Mail') ? 'text': 'number'}
          key={f.id}
          className={`p-1 my-3 w-100 mr-3 ${f.id}`}
          size='16px'
          placeholder={f.placeholder}
          value={this.state[f.id]}
          onChange={(e) => this.hc(e, f.id)}
        />
      ))}
    </form>
  )
  render() {
    const {
      props: {control: { onCancel, onSaveContact }}
    } = this
    return (
      <div>
        <this.ContactInput
          fields={[
            { placeholder: 'Name', id: 'Name' },
            { placeholder: 'Phone number', id: 'Messages' },
            { placeholder: 'Whatsapp number', id: 'WhatsApp' },
            { placeholder: 'Email', id: 'Mail' },
          ]}
        />
        <div className='d-flex justify-content-between'>
          <Button outline primary border
          style={{ padding: '8px 44px'}}
          onClick={onCancel}
          >Cancel</Button>
          <Button primary border
          onClick={(ev) => onSaveContact(ev, {...this.state})}
          >Save Contact</Button>
        </div>
      </div>
    )
  }
}
const NewContact = ({newContact, control: {onTypeClick}}) => (
  <div>
    <div className="p-2" style={{ color: '#1e93ff' }}>{newContact.name}</div>
    <div className="py-2" style={{ cursor: 'pointer' }} onClick={(ev) => onTypeClick(ev, 'WhatsApp', newContact)}>
      <Icon icon="WhatsApp" alt="WhatsApp" />
      <span className="px-3">WhatsApp</span>
    </div>
    <div className="py-2" style={{ cursor: 'pointer' }} onClick={(ev) => onTypeClick(ev, 'Mail', newContact)}>
      <Icon icon="Mail" alt="Mail" />
      <span className="px-3">Email</span>
    </div>
    <div className="py-2" style={{ cursor: 'pointer' }} onClick={(ev) => onTypeClick(ev, 'Messages', newContact)}>
      <Icon icon="Messages" alt="Messages" />
      <span className="px-3">Messages</span>
    </div>
  </div>
)
class ActionBar extends Component {
  state = {
    contactAdd: false,
   contacts: [],
    newContact: null,
  }
  newContactBoxControl = {
    onCancel: (ev) => {
      ev.preventDefault()
      this.toggle.AddContact()
    },
    onSaveContact: (ev, newContact) => {
      ev.preventDefault()
      const contact = {...newContact, id: shortid.generate()}
      if (contact.phone && !contact.whatsapp) {
       contact.whatsapp =contact.phone
      }
      this.props.saveNewContact({contact })
      this.toggle.AddContact()
    }
  }
  addContact = (contact) => {
    this.setState(({contacts}) => ({
     contacts: [...contacts,contact],
    }))
  }
  searchBoxControl = {
    onOldContactClick: (ev, oldContact) => {
      ev.preventDefault()
      const contact = {...oldContact}
      const types = ['Messages', 'WhatsApp', 'Mail'].map(t =>contact[t] ? t : null).filter(t => t !== null)
      if (types.length > 1) {
        this.setState({
          newContact:contact
        })
      } else {
        this.addContact({...contact, type: types[0]})
      }
    }
  }
  newContactControl = {
    onTypeClick: (ev, type, newContact) => {
      ev.preventDefault()
      const contact = {...newContact, type}
      this.addContact(contact)
      this.setState({
        newContact: null
      })
    }
  }
  onClearClick = (ev) => {
    ev.preventDefault()
    if (!this.state.contactAdd) {
      this.setState({
       contacts: [],
        newContact: null,
      })
    }
  }
  toggle = {
    AddContact: () => {
      this.setState(({ contactAdd })=> ({
        contactAdd: !contactAdd
      }))
    },
  }
  render() {
    const {
      toggle,
      newContactBoxControl,
      newContactControl,
      searchBoxControl,
      onClearClick,
      state: { contactAdd,contacts, newContact },
      props: { oldContacts }
    } = this
    return (
      <div className='ActionBar'
      style={{ 
        padding: '32px',
        width: '360px',
        background: '#fcfdfd',
        height: '100%'
      }}>
        <div className='d-flex justify-content-between border-bottom pb-4'>
          <Button outline primary border>Download PDF</Button>
          <Button primary border>Send Invoice</Button>
        </div>
        <div className='border-bottom py-4'>
          <div className='d-flex justify-content-between'>
            <Button primary outline text onClick={toggle.AddContact}>+ Add Contact</Button>
            <Button primary outline text danger onClick={(ev) => onClearClick(ev)}>Clear</Button>
          </div>
        </div>
        {contactAdd ?
          <NewContactBox control={newContactBoxControl}/> :
          <React.Fragment>
            <SearchBox contacts={contacts} oldContacts={oldContacts} control={searchBoxControl}/>
            {newContact ? <NewContact newContact={newContact} control={newContactControl}/>: null}
            <RecContactBox contacts={contacts}/>
          </React.Fragment>
        }
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  saveNewContact: payload => dispatch({ type: SAVE_CONTACT, payload })
})

const mapStateToProps = (state) => ({
  oldContacts: state.contacts.contactList,
  prevInvoices: state.prevInvoices,
  contactTypes: state.contactTypes
})
export default connect(mapStateToProps, mapDispatchToProps)(ActionBar)
