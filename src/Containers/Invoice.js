import React, { Component } from 'react'
// import { connect } from 'react-redux'

import { Button, Input, Title6 } from '../Components/Base'
import { Table } from '../Components/Table'

class Invoice extends Component {

  state = {
    table: {
      define: [
        {
          mutable: true,
          key: "item",
          placeholder: "Item",
          width: 2
        },
        {
          mutable: true,
          key: "desc",
          placeholder: "Description",
          width: 5
        },
        {
          mutable: true,
          key: "rate",
          placeholder: "Rate",
          width: 2,
          default: 0,
          cast: (v) => parseFloat(v, 10)
        },
        {
          mutable: true,
          key: "quantity",
          placeholder: "Qty",
          default: 1,
          width: 1,
          cast: (v) => parseInt(v, 10)
        },
        {
          key: "total",
          placeholder: 'Linetotal',
          compute: data => data.rate * data.quantity,
          width: 2,
          style: {
            textAlign: 'right'
          }
        }
      ],
      data: [
        { item: "Landing Page", desc: "Revised landing page design", rate: 1500, quantity: 1 },
        { item: "Prototypes", desc: "Animated prototypes", rate: 1200, quantity: 1 }
      ]
    },
    subTable: [
      {
        title: 'subtotal',
        compute: (data) => data.map(({ rate, quantity = 1 }) => rate * quantity).reduce((a,c) => a+c, 0)
      },
      {
        title: 'taxes',
        compute: (data) => data.map(({ rate, quantity = 1, tax = 0 }) => (rate * quantity * tax / 100)).reduce((a,c) => a + c, 0)
      },
      {
        title: 'total',
        style: {color: '#00b200'},
        compute: (data) => data.map(({ rate, quantity = 1, tax = 0 }) => (rate * quantity * (1 + tax / 100))).reduce((a,c) => a + c, 0)
      }
    ],
    customer: {
      name: 'Wise Systems',
      address: [
        '945 Lombard St',
        'San Francisco CA 94133'
      ]
    }
  }

  updateTable = (table) => {
    this.setState({ table })
  }

  render() {

    return (
      <div className="invoice w-100 p-4 border-right">
        <header className="d-flex justify-content-between align-items-start">
          <h4 className="mb-4"><Input value={'New Invoice'} onChange={() => {}}/></h4>
          <Button primary outline text>+ New Invoice</Button>
        </header>
        <div className="d-flex justify-content-between">
          <div className="left d-flex flex-column">
            <Title6 className="mb-1">Billed to</Title6>
            <Input size="22px" value={this.state.customer.name} onChange={() => {}}/>
            <Input className="mt-2" color="#aaaaaf" value={this.state.customer.address[0]} onChange={() => {}}/>
            <Input color="#aaaaaf" value={this.state.customer.address[1]} onChange={() => {}}/>
          </div>
          <div className="right">
            <Title6 className="float-right">Date</Title6>
            <span className="d-block" style={{ fontWeight: '500' }}>{(new Date()).toLocaleDateString()}</span>
          </div>
        </div>
        <h4 className="pb-2 mt-3 mb-0">Summary</h4>     
        <Table
        updateTable={this.updateTable}
        table={this.state.table}
        subTable={this.state.subTable}/>
      </div>
    )
  }
}

export default Invoice