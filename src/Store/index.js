import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { persistentStore } from 'redux-pouchdb-plus'
import PouchDB from 'pouchdb'

import createRootReducer from './Reducers'

import { routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from "history"

const db = new PouchDB('invoicer')
export const history = createBrowserHistory()

export const configureStore = function configureStore(preloadedState) {
  const store = createStore(
    createRootReducer(history), // root reducer with router state
    preloadedState,
    composeWithDevTools(
      applyMiddleware(
        routerMiddleware(history),
        createLogger(),
      ),
      persistentStore({db}),
    ),
  )
  return store
}

export default configureStore
