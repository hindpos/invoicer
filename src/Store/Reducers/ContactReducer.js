import { SAVE_CONTACT, CLEAR } from 'Store/Constants/actionTypes'

const ContactReducer = (state, action) => {
  switch(action.type) {
    case SAVE_CONTACT:
      return {
        ...state,
        contactList: (state.contactList || []).concat([action.payload.contact]),
      }
    case CLEAR:
      return { contactList: [] }
    default:
      return state || { contactList: [] }
  }
}

export default ContactReducer
