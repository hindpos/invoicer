const PrevInvoicesReducer = () => ([
  {
    number: 'Invoice #001',
    status: 'Paid'
  },
  {
    number: 'Invoice #002',
    status: 'Paid'
  },
  {
    number: 'Invoice #001',
    status: 'Pending'
  },
])

export default PrevInvoicesReducer