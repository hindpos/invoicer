import FileTreeReducer from './FileTreeReducer'
import PrevInvoicesReducer from './PrevInvoicesReducer'
import ContactReducer from './ContactReducer'
import ContactTypesReducer from './ContactTypesReducer'
import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { persistentReducer } from 'redux-pouchdb-plus'


const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  fileTree: persistentReducer(FileTreeReducer),
  prevInvoices: persistentReducer(PrevInvoicesReducer),
  contacts: persistentReducer(ContactReducer),
  contactTypes: persistentReducer(ContactTypesReducer),
})

export default createRootReducer