const FileTreeReducer = () => ({
  1: { open: true, name: 'Products', contents: {
    2: {name: 'Apple Inc', contents: [], type: 'c'},
    7: {name: 'Wise Sys', contents: [], type: 'c'}
  }, type: 'd'},
  8: {name: 'Services', contents: {
    3: {name: 'Yellow Pages', contents: {}, type: 'c'},
    4: {name: 'Instacart Inc', contents: {}, type: 'c'}
  }, type: 'd'}
})
export default FileTreeReducer