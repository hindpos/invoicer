import React, { Component } from 'react'

import Sidebar from './Containers/Sidebar'
import Invoice from './Containers/Invoice'
import ActionBar from './Containers/ActionBar'

// import Auth from './Pages/Auth'

const Main = () => {
  return (
    <main className="d-flex"
    style={{
      minHeight: '100vh'
    }}>
      <Invoice />
      <div>
        <ActionBar />
      </div>
    </main>
  )
}

class App extends Component {
  render() {
    return (
      <div className="App d-flex">
        <div>
          <Sidebar />
        </div>
        <div className="right-content w-100">
          <Main />
        </div>
        {/* <PopupHandler /> */}
        {/* <Auth/> */}
      </div>
    )
  }
}


export default App
